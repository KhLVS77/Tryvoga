# ###################################################################
# Конфігуратор для програми відслудковування стану повітряних тривог в Україні
#
#                   Версія 2.0
# Дозволяє в графічному режимі обирати області для відслідковування
#
# Автор: KhLVS (matrix: @lvskh:matrix.org)
# Репозиторій: https://codeberg.org/KhLVS77/Tryvoga/src/branch/V2
#
#   This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License along
#      with this program; if not, write to the Free Software Foundation, Inc.,
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
#   https://www.gnu.org/licenses/gpl-3.0.html
#
# ###################################################################

import json
import tkinter as tk
from tkinter import Frame
from tkinter import messagebox
import sys
import os

File='setup.json' # файл в якому будуть зберігатися налаштування 
Regions={}
List=[]

def main():
    def MySave():
        List.clear()
       
        if var1.get()==1:   List.append("Вінницька область")
        if var2.get()==1:   List.append("Волинська область")
        if var3.get()==1:   List.append("Дніпропетровська область")
        if var4.get()==1:   List.append("Донецька область")
        if var5.get()==1:   List.append("Житомирська область")
        if var6.get()==1:   List.append("Закарпатська область")
        if var7.get()==1:   List.append("Запорізька область")
        if var8.get()==1:   List.append("Івано-Франківська область")
        if var9.get()==1:   List.append("Київська область")
        if var10.get()==1:  List.append("Кіровоградська область")
        if var11.get()==1:  List.append("Луганська область")
        if var12.get()==1:  List.append("Львівська область")
        if var13.get()==1:  List.append("Миколаївська область")
        if var14.get()==1:  List.append("Одеська область")
        if var15.get()==1:  List.append("Полтавська область")
        if var16.get()==1:  List.append("Рівненська область")
        if var17.get()==1:  List.append("Сумська область")
        if var18.get()==1:  List.append("Тернопільська область")
        if var19.get()==1:  List.append("Харківська область")
        if var20.get()==1:  List.append("Херсонська область")
        if var21.get()==1:  List.append("Хмельницька область")
        if var22.get()==1:  List.append("Черкаська область")
        if var23.get()==1:  List.append("Чернівецька область")
        if var24.get()==1:  List.append("Чернігівська область")
        if var25.get()==1:  List.append("м. Київ")
        if var26.get()==1:  List.append("АР Крим")
        
        Regions["region"]=List
        
        with open(File, 'w') as outfile:
            outfile.write(json.dumps(Regions))
        
        messagebox.showinfo("info", "Перелік областей оновлено...")
        sys.exit()
        
    def MyExit():
        at=messagebox.askquestion("Вихід ?", "Впевнені, що бажаєте вийти?")
        if (at == 'yes'):
            sys.exit()
        else:
            return
            
    window = tk.Tk()
    window.resizable(False, False)
    window.title('Виберіть області...')
    common_frame1 = Frame(window)
    common_frame1.grid(row=0, column=0)

    label=tk.Label(common_frame1, text="Виберіть області, що Вас цікавлять...", font=("Roboto Bold", 10))
    label.grid(column=0, row=0, padx=10, pady=10, columnspan=2)
#
    if os.path.exists(File) == False:
        with open(File, 'w') as Ftmp:
            Ftmp.write('{"region": []}')
            Ftmp.close()
        
    with open(File) as f:
        file_content = f.read()
        Rez = json.loads(file_content)
    R=Rez['region']

    var1 = tk.IntVar()
    c1 = tk.Checkbutton(common_frame1, text=' Вінницька область',variable=var1, onvalue=1, offvalue=0)
# !   c1.select() # включили чекбокс
# !   print (c1.cget("text")) читаем техt
    if c1.cget("text").lstrip() in R:
        c1.select()
    c1.grid(column=0, row=1, sticky="w")

    var2 = tk.IntVar()
    c2 = tk.Checkbutton(common_frame1, text=' Волинська область',variable=var2, onvalue=1, offvalue=0)
    if c2.cget("text").lstrip() in R:
        c2.select()
    c2.grid(column=0, row=2, sticky="w")
    
    var3 = tk.IntVar()
    c3 = tk.Checkbutton(common_frame1, text=' Дніпропетровська область',variable=var3, onvalue=1, offvalue=0)
    if c3.cget("text").lstrip() in R:
        c3.select()
    c3.grid(column=0, row=3, sticky="w")
    
    var4 = tk.IntVar()
    c4 = tk.Checkbutton(common_frame1, text=' Донецька область',variable=var4, onvalue=1, offvalue=0)
    if c4.cget("text").lstrip() in R:
        c4.select()
    c4.grid(column=0, row=4, sticky="w")
    
    var5 = tk.IntVar()
    c5 = tk.Checkbutton(common_frame1, text=' Житомирська область',variable=var5, onvalue=1, offvalue=0)
    if c5.cget("text").lstrip() in R:
        c5.select()
    c5.grid(column=0, row=5, sticky="w")
    
    var6 = tk.IntVar()
    c6 = tk.Checkbutton(common_frame1, text=' Закарпатська область',variable=var6, onvalue=1, offvalue=0)
    if c6.cget("text").lstrip() in R:
        c6.select()
    c6.grid(column=0, row=6, sticky="w")
    
    var7 = tk.IntVar()
    c7 = tk.Checkbutton(common_frame1, text=' Запорізька область',variable=var7, onvalue=1, offvalue=0)
    if c7.cget("text").lstrip() in R:
        c7.select()
    c7.grid(column=0, row=7, sticky="w")
    
    var8 = tk.IntVar()
    c8 = tk.Checkbutton(common_frame1, text=' Івано-Франківська область',variable=var8, onvalue=1, offvalue=0)
    if c8.cget("text").lstrip() in R:
        c8.select()
    c8.grid(column=0, row=8, sticky="w")
    
    var9 = tk.IntVar()
    c9 = tk.Checkbutton(common_frame1, text=' Київська область',variable=var9, onvalue=1, offvalue=0)
    if c9.cget("text").lstrip() in R:
        c9.select()
    c9.grid(column=0, row=9, sticky="w")
    
    var10 = tk.IntVar()
    c10 = tk.Checkbutton(common_frame1, text=' Кіровоградська область',variable=var10, onvalue=1, offvalue=0)
    if c10.cget("text").lstrip() in R:
        c10.select()
    c10.grid(column=0, row=10, sticky="w")
    
    var11 = tk.IntVar()
    c11 = tk.Checkbutton(common_frame1, text=' Луганська область',variable=var11, onvalue=1, offvalue=0)
    if c11.cget("text").lstrip() in R:
        c11.select()
    c11.grid(column=0, row=11, sticky="w")
    
    var12 = tk.IntVar()
    c12 = tk.Checkbutton(common_frame1, text=' Львівська область',variable=var12, onvalue=1, offvalue=0)
    if c12.cget("text").lstrip() in R:
        c12.select()
    c12.grid(column=0, row=12, sticky="w")
    
    var13 = tk.IntVar()
    c13 = tk.Checkbutton(common_frame1, text=' Миколаївська область',variable=var13, onvalue=1, offvalue=0)
    if c13.cget("text").lstrip() in R:
        c13.select()
    c13.grid(column=1, row=1, sticky="w")
    
    var14 = tk.IntVar()
    c14 = tk.Checkbutton(common_frame1, text=' Одеська область',variable=var14, onvalue=1, offvalue=0)
    if c14.cget("text").lstrip() in R:
        c14.select()
    c14.grid(column=1, row=2, sticky="w")
    
    var15 = tk.IntVar()
    c15 = tk.Checkbutton(common_frame1, text=' Полтавська область',variable=var15, onvalue=1, offvalue=0)
    if c15.cget("text").lstrip() in R:
        c15.select()
    c15.grid(column=1, row=3, sticky="w")
    
    var16 = tk.IntVar()
    c16 = tk.Checkbutton(common_frame1, text=' Рівненська область',variable=var16, onvalue=1, offvalue=0)
    if c16.cget("text").lstrip() in R:
        c16.select()
    c16.grid(column=1, row=4, sticky="w")
    
    var17 = tk.IntVar()
    c17 = tk.Checkbutton(common_frame1, text=' Сумська область',variable=var17, onvalue=1, offvalue=0)
    if c17.cget("text").lstrip() in R:
        c17.select()
    c17.grid(column=1, row=5, sticky="w")
    
    var18 = tk.IntVar()
    c18 = tk.Checkbutton(common_frame1, text=' Тернопільська область',variable=var18, onvalue=1, offvalue=0)
    if c18.cget("text").lstrip() in R:
        c18.select()
    c18.grid(column=1, row=6, sticky="w")
    
    var19 = tk.IntVar()
    c19 = tk.Checkbutton(common_frame1, text=' Харківська область',variable=var19, onvalue=1, offvalue=0)
    if c19.cget("text").lstrip() in R:
        c19.select()
    c19.grid(column=1, row=7, sticky="w")
    
    var20 = tk.IntVar()
    c20 = tk.Checkbutton(common_frame1, text=' Херсонська область',variable=var20, onvalue=1, offvalue=0)
    if c20.cget("text").lstrip() in R:
        c20.select()
    c20.grid(column=1, row=8, sticky="w")
    
    var21 = tk.IntVar()
    c21 = tk.Checkbutton(common_frame1, text=' Хмельницька область',variable=var21, onvalue=1, offvalue=0)
    if c21.cget("text").lstrip() in R:
        c21.select()
    c21.grid(column=1, row=9, sticky="w")
    
    var22 = tk.IntVar()
    c22 = tk.Checkbutton(common_frame1, text=' Черкаська область',variable=var22, onvalue=1, offvalue=0)
    if c22.cget("text").lstrip() in R:
        c22.select()
    c22.grid(column=1, row=10, sticky="w")
    
    var23 = tk.IntVar()
    c23 = tk.Checkbutton(common_frame1, text=' Чернівецька область',variable=var23, onvalue=1, offvalue=0)
    if c23.cget("text").lstrip() in R:
        c23.select()
    c23.grid(column=1, row=11, sticky="w")
    
    var24 = tk.IntVar()
    c24 = tk.Checkbutton(common_frame1, text=' Чернігівська область',variable=var24, onvalue=1, offvalue=0)
    if c24.cget("text").lstrip() in R:
        c24.select()
    c24.grid(column=1, row=12, sticky="w")
    
    var25 = tk.IntVar()
    c25 = tk.Checkbutton(common_frame1, text=' м. Київ',variable=var25, onvalue=1, offvalue=0)
    if c25.cget("text").lstrip() in R:
        c25.select()
    c25.grid(column=0, row=13, sticky="w")
    
    var26 = tk.IntVar()
    c26 = tk.Checkbutton(common_frame1, text=' АР Крим',variable=var26, onvalue=1, offvalue=0)
    if c26.cget("text").lstrip() in R:
        c26.select()
    c26.grid(column=1, row=13, sticky="w")

    common_frame2 = Frame(window)
    common_frame2.grid(row=1, column=0)
    
    btn = tk.Button(common_frame2, text="Зберегти...", command=MySave)
    btn.grid(column=0, row=0, padx=10, pady=10, sticky="e")
    
    btn = tk.Button(common_frame2, text="Вийти...", command=MyExit)
    btn.grid(column=1, row=0, padx=10, pady=10, sticky="w")
    
    window.mainloop()
    
if __name__ == '__main__':
    main()

