# ###################################################################
# Програма для відслудковування стану повітряних тривог в Україні
#                   Версія 4.0
# Автор: KhLVS (matrix: @lvskh:matrix.org)
# Репозиторій: https://codeberg.org/KhLVS77/Tryvoga/src/branch/V2
#
#   This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License along
#      with this program; if not, write to the Free Software Foundation, Inc.,
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
#   https://www.gnu.org/licenses/gpl-3.0.html
#
# ###################################################################

import json
import requests
import os
import time
import datetime
import socket
import subprocess
from tkinter import messagebox
import sys

File='setup.json' # файл в якому будуть зберігатися налаштування 

def My_init():
# початкова ініціалізація. Зчитування переліку областей з File
    currpath = os.path.dirname(os.path.realpath(__file__))
    if os.path.exists(File) == False:
        arg=currpath+"/Configurator.py"
        subprocess.run(["python3", arg])
    
    with open(File) as f:
       file_content = f.read()
       Rez = json.loads(file_content)
    if len(Rez['region']) == 0:
       messagebox.showerror("Помилка !", "Встановіть перелік областей та перезапустіть програму... ")
       currpath = os.path.dirname(os.path.realpath(__file__))
       arg=currpath+"/Configurator.py"
       subprocess.run(["python3", arg])   
       sys.exit()
    return(Rez['region'])
    
    
def f():
#Перевірка наявності інтернету
   try:
        socket.gethostbyaddr('www.google.com')
   except:
        time.sleep(20)
        return False
   return True

def main():   
    Reg=[]
    Reg=My_init()
    
    now = datetime.datetime.now()
    MyText2="'Програму моніторингу тривоги, запущено..."+" "+str(now.strftime("%d.%m.%Y %H:%M:%S"))+"'"
    Log = open(os.path.abspath(os.curdir)+'/Tryvoga.log', 'a')
    Log.write('\n[-SYS-]'+MyText2 + '\n')
    Log.close()
    
    Message2="notify-send 'Інформація...' --urgency=normal --icon="+os.path.abspath(os.curdir)+"/src/img/security-medium.png 'Програму моніторингу тривоги, запущено...' "
    os.system(Message2)
    
    inetYES=True
    while True:
        inetYES=f()

        # Перевіряємо наявність інтернету. Якшо нема, то виводимо відповідне повідомлення, зупиняємося на хвилину, а потім починаємо цикл спочатку
        if inetYES==False:
            now = datetime.datetime.now()
            MyText="'Проблема з Інтернетом ..."+" "+str(now.strftime("%d.%m.%Y %H:%M:%S"))+"'"
            Log = open(os.path.abspath(os.curdir)+'/Tryvoga.log', 'a')
            Log.write('[-SYS-] '+'\t'+MyText + '\n')
            Log.close()
            Message="notify-send 'Інформація...' --urgency=normal --icon="+os.path.abspath(os.curdir)+"/src/img/network-wired-disconnected.png 'Проблема з Інтернетом ...' "
            os.system(Message)
            time.sleep(60)
            continue
        
        response = 0
        now = datetime.datetime.now()
        try:
            response = requests.get("https://emapa.fra1.cdn.digitaloceanspaces.com/statuses.json")
        except:
            time.sleep(30)
            continue
        todos = json.loads(response.text)
        
        for Region in Reg:
            trevoga=todos['states'][Region]['enabled']
            dic=todos['states'][Region]['districts']
            districts=todos['states'][Region]['districts'].keys()
            for t in districts:
                if dic.get(t)['enabled']== True:
#                    print("Тривога! "+ Region+", "+ t)
                    MyText="'"+Region+", "+t+". Всі по норах !"+"   "+str(now.strftime("%d.%m.%Y %H:%M:%S"))+"'"
                    Log = open(os.path.abspath(os.curdir)+'/Tryvoga.log', 'a')
                    Log.write('[-!!!-]\t'+MyText + '\n')
                    Log.close()
                    Message="notify-send '"+Region+",\n "+t+"' --urgency=normal --icon="+os.path.abspath(os.curdir)+"/src/img/software-update-urgent.png 'Повітряна тривога !' "
                    os.system(Message)
             
            
            now = datetime.datetime.now()
            
            if (trevoga==True):
                MyText="'"+Region+". Всі по норах !"+"   "+str(now.strftime("%d.%m.%Y %H:%M:%S"))+"'"
                Log = open(os.path.abspath(os.curdir)+'/Tryvoga.log', 'a')
                Log.write('[-!!!-]\t'+MyText + '\n')
                Log.close()
                Message="notify-send '"+Region+"' --urgency=normal --icon="+os.path.abspath(os.curdir)+"/src/img/software-update-urgent.png 'Повітряна тривога !' "
                os.system(Message)
            
            if (trevoga==False):
                MyText="'"+Region+". На даний момент, тривога відсутня."+"    "+str(now.strftime("%d.%m.%Y %H:%M:%S"))+"'"
                Message="notify-send '"+Region+"' --urgency=normal --icon="+os.path.abspath(os.curdir)+"/src/img/emblem-default.png 'На даний момент, тривога відсутня' "
                os.system(Message)
                Log = open(os.path.abspath(os.curdir)+'/Tryvoga.log', 'a')
                Log.write('[-ОК!-]\t'+MyText + '\n')
                Log.close()
        time.sleep(30)

if __name__ == '__main__':
    main()
    
